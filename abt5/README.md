
# Abteilung 5 - Filzgolf

**Regelwerk:** [S13 - NormFilz](https://www.minigolfsport.de/pdf/Download/Regelwerk/s13_normungsbestimmungenfilzgolf_2016-03.pdf)

## Bahnen
|Bahn|Name                 |Dateiname      | Abgeschlossen |
|----|---------------------|---------------|---------------|
|1   |Einfachtor           |abt5_b1.svg    |:heavy_check_mark:|  
|1   |Einfachtor (steigend)|abt5_b1_a2.svg |:heavy_check_mark:|   
|2   |Doppeltor            |abt5_b2.svg    |:heavy_check_mark:|  
|2   |Doppeltor (steigend) |abt5_b2_a2.svg |:heavy_check_mark:|  
|3   |Kästen               |abt5_b3.svg    |:heavy_check_mark:|  
|4   |Deutscher Absatz     |abt5_b4.svg    |:heavy_check_mark:|  
|4   |Deutscher Absatz (Plateau)|abt5_b4_a2.svg|:heavy_check_mark:|
|5   |Optische Täuschung   |abt5_b5.svg    |:heavy_check_mark:|  
|6   |Steigung mit Mittelloch|abt5_b6.svg  |:heavy_check_mark:|  
|6   |Steigung mit Mittelloch (ohne Zielfeld)|abt5_b6_o.svg|:heavy_check_mark:|  
|6   |Geldkasten           |abt5_b6_a2.svg |:heavy_check_mark:|  
|6   |Geldkasten (ohne Zielfeld)|abt5_b6_a2_o.svg|:heavy_check_mark:|  
|7   |Briefkasten          |abt5_b7.svg    |:heavy_check_mark:|  
|8   |Gentleman            |abt5_b8.svg    |:heavy_check_mark:|  
|8   |Gentleman (ohne Zielfeld)|abt5_b8_o.svg|:heavy_check_mark:|  
|9   |Örkelljunga          |abt5_b9.svg    |:heavy_check_mark:|  
|9   |Örkelljunga (ohne Zielfeld)|abt5_b9_o.svg|:heavy_check_mark:|  
|10  |Winkel               |abt5_b10.svg   |:heavy_check_mark:|   
|10  |Winkel (Spielgelbild)|abt5_b10_s.svg |:heavy_check_mark:|    
|11  |Winkel mit Verlängerung|abt5_b11.svg |:heavy_check_mark:|    
|11  |Winkel mit Verlängerung (Spielgelbild)|abt5_b11.svg|:heavy_check_mark:|    
|11  |Winkel mit Umgehung  |abt5_b11_a2.svg|:heavy_check_mark:|  
|11  |Winkel mit Umgehung (Spielgelbild)|abt5_b11_a2_s.svg|:heavy_check_mark:|  
|12  |Mittelhügel          |abt5_b12.svg   |:heavy_check_mark:|  
|13  |Seitentore           |abt5_b13.svg   |:heavy_check_mark:|  
|13  |Seitentore (steigend)|abt5_b13_a2.svg|:heavy_check_mark:|  
|14  |Mulde                |abt5_b14.svg   |:heavy_check_mark:|  
|14  |Mulde (ohne Zielfeld)|abt5_b14_o.svg |:heavy_check_mark:|  
|15  |Fischgräte           |abt5_b15.svg   |:heavy_check_mark:|  
|16  |Rinne                |abt5_b16.svg   |:heavy_check_mark:|  
|16  |Rinne (steigend)     |abt5_b16_a2.svg|:heavy_check_mark:|  
|17  |Steigung mit Seitenloch|abt5_b17.svg |:heavy_check_mark:|  
|17  |Steigung mit Seitenloch (ohne Zielfeld)|abt5_b17_o.svg|:heavy_check_mark:|  
|18  |Brücke               |abt5_b18.svg   |:heavy_check_mark:|  
|19  |Hügel mit Tor        |abt5_b19.svg   |:heavy_check_mark:|  
|20  |Blitz                |abt5_b20.svg   |:heavy_check_mark:|  
|21  |Graben               |abt5_b21.svg   |:heavy_check_mark:|  
|22  |Schräger Hügel mit Tor|abt5_b22.svg  |:heavy_check_mark:|  
|22  |Schräger Hügel mit Tor (Spielgelbild)|abt5_b22_s.svg|:heavy_check_mark:|  
|23  |Ass-Box              |abt5_b23.svg   |:heavy_check_mark:|  
|24  |Schwedischer Absatz  |abt5_b24.svg   |:heavy_check_mark:|  
|24  |Schwedischer Absatz (Plateau)|abt5_b24_a2.svg|:heavy_check_mark:|  
|25  |Kreuz                |abt5_b25.svg   |:heavy_check_mark:|  
|26  |Möllberg-Mulde       |abt5_b26.svg   |:heavy_check_mark:|  
|27  |Steigung mit Vertikalloch|abt5_b27.svg|:heavy_check_mark:|  
|27  |Steigung mit Vertikalloch (ohne Zielfeld)|abt5_b27_o.svg|:heavy_check_mark:|  
|28  |Hufeisen             |abt5_b28.svg   |:heavy_check_mark:|  
|28  |Hufeisen (ohne Zielfeld)|abt5_b28_o.svg|:heavy_check_mark:|  
|29  |Zwillingstore        |abt5_b29.svg   |:heavy_check_mark:|  
|30  |Stationäre Waage     |abt5_b30.svg   |:heavy_check_mark:|  
|30  |Stationäre Waage (ohne Zielfeld)|abt5_b30_o.svg|:heavy_check_mark:|  
|31  |Mittelzielfeld       |abt5_b31.svg   |:heavy_check_mark:|  
|32  |Passage              |abt5_b32.svg   |:heavy_check_mark:|  
