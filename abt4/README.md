
# Abteilung 4 - Sterngolf

**Regelwerk:** [S16 - NormMOS-S](https://www.minigolfsport.de/pdf/Download/Regelwerk/s16_normungsbestimmungensterngolf_2011-03.pdf)

## Bahnen
|Bahn|Name                 |Dateiname      | Abgeschlossen    |
|----|---------------------|---------------|------------------|
|1   |Bahn 1               |abt4_b1.svg    |:heavy_check_mark:|
|2   |Bahn 2               |abt4_b2.svg    |:heavy_check_mark:|
|3   |Bahn 3               |abt4_b3.svg    |:heavy_check_mark:|
|4   |Bahn 4               |abt4_b4.svg    |:heavy_check_mark:|
|5   |Bahn 5               |abt4_b5.svg    |:heavy_check_mark:|
|6   |Bahn 6               |abt4_b6.svg    |:heavy_check_mark:|
|7   |Bahn 7               |abt4_b7.svg    |:heavy_check_mark:|
|8   |Bahn 8               |abt4_b8.svg    |:heavy_check_mark:|
|9   |Bahn 9               |abt4_b9.svg    |:heavy_check_mark:|
|10  |Bahn 10              |abt4_b10.svg   |:heavy_check_mark:|
|11  |Bahn 11              |abt4_b11.svg   |:heavy_check_mark:|
|12  |Bahn 12              |abt4_b12.svg   |:heavy_check_mark:|
|13  |Bahn 13              |abt4_b13.svg   |:heavy_check_mark:|
|14  |Bahn 14              |abt4_b14.svg   |:heavy_check_mark:|
|15  |Bahn 15              |abt4_b15.svg   |:heavy_check_mark:|
|16  |Bahn 16              |abt4_b16.svg   |:heavy_check_mark:|
|17  |Bahn 17              |abt4_b17.svg   |:heavy_check_mark:|
|18  |Bahn 18              |abt4_b18.svg   |:heavy_check_mark:|
