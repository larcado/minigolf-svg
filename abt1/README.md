# Abteilung 1 - Beton

**Regelwerk:** [S11 - NormBeton](https://www.minigolfsport.de/pdf/Download/Regelwerk/s11_normungsbestimmungenbeton_2018-01.pdf)

## Bahnen
|Bahn|Name                 |Dateiname      | Abgeschlossen |
|----|---------------------|---------------|---------------|
|1   |Bahn 1               |abt1_b1.svg    |:heavy_check_mark:|
|2   |Bahn 2               |abt1_b2.svg    |:heavy_check_mark:|
|3   |Bahn 3               |abt1_b3.svg    |:heavy_check_mark:|
|4   |Bahn 4               |abt1_b4.svg    |:heavy_check_mark:|
|4   |Bahn 4 (Spiegelbild) |abt1_b4_s.svg  |:heavy_check_mark:|
|5   |Bahn 5               |abt1_b5.svg    |:heavy_check_mark:|
|5   |Bahn 5 (Spiegelbild) |abt1_b5_s.svg  |:heavy_check_mark:|
|6   |Bahn 6               |abt1_b6.svg    |:heavy_check_mark:|
|7   |Bahn 7               |abt1_b7.svg    |:heavy_check_mark:|
|8   |Bahn 8               |abt1_b8.svg    |:heavy_check_mark:|
|9   |Bahn 9               |abt1_b9.svg    |:heavy_check_mark:|
|9   |Bahn 9 (Spiegelbild) |abt1_b9_s.svg  |:heavy_check_mark:|
|10  |Bahn 10              |abt1_b10.svg   |:heavy_check_mark:|
|11  |Bahn 11              |abt1_b11.svg   |:heavy_check_mark:|
|11  |Bahn 11 (Spiegelbild)|abt1_b11_s.svg |:heavy_check_mark:|
|12  |Bahn 12              |abt1_b12.svg   |:heavy_check_mark:|
|12  |Bahn 12 (Spiegelbild)|abt1_b12_s.svg |:heavy_check_mark:|
|13  |Bahn 13              |abt1_b13.svg   |:heavy_check_mark:|
|13  |Bahn 13 (Spiegelbild)|abt1_b13_s.svg |:heavy_check_mark:|
|14  |Bahn 14              |abt1_b14.svg   |:heavy_check_mark:|
|15  |Bahn 15              |abt1_b15.svg   |:heavy_check_mark:|
|16  |Bahn 16              |abt1_b16.svg   |:heavy_check_mark:|
|16  |Bahn 16 (Spiegelbild)|abt1_b16_s.svg |:heavy_check_mark:|
|17  |Bahn 17              |abt1_b17.svg   |:heavy_check_mark:|
|18  |Bahn 18              |abt1_b18.svg   |:heavy_check_mark:|
